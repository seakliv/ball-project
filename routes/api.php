<?php

use Illuminate\Http\Request;
use App\Http\Services\OrderService;

Route::get('/','API\Auth\LoginController@login');

Route::post('login', 'API\Auth\LoginController@login');

Route::middleware('auth:api')->group( function () {
    Route::post('logout','API\Auth\LoginController@logout');
    
    Route::get('pusher/beam-token',function(){
        $beamsToken = notificationInstance()->generateToken((string)auth()->id());
        return response()->json($beamsToken);
    });

    Route::group(['namespace' => 'API'],function(){
        Route::group(['namespace' => 'Cycle'],function(){
            Route::get('cycles','CycleController@getAllCycles');
            Route::get('cycles/tickets','CycleController@getAllInvolvedCycles');
            Route::get('cycles/time-remain','CycleController@getTimeRemain');
            Route::get('cycles/mine','CycleController@getMyCycle');
            Route::get('cycles/finished','CycleController@getfinishedCycle');
            Route::get('cycles/active','CycleController@getActiveCycle');
            Route::get('cycles/{id}','CycleController@getCycleById');
        });

        Route::group(['namespace' => 'Order'],function(){
            Route::post('orders','OrderController@createOrder');
            Route::get('cycles/{id}/tickets','OrderController@getTicketsByCycle');
            Route::get('tickets/{ticket}/orders','OrderController@getOrdersByTicket');
        });

        Route::group(['namespace' => 'User'], function(){
            Route::get('users/accounts', 'AccountController@getUserAccounts');
            //Params: to_user_accountno, amount, pay_password
            Route::post('users/cash-account/transfer', 'AccountController@tranferToOtherCashAccount');
            //Params: name
            Route::get('users/get-user-by-accountno','UserController@getUserByAccount');

            //Params: key=winmoney|commission, amount
            Route::post('users/transfer-to-main-balance','AccountController@tranferToMainBalance');
            
            //Params: key = winmoney|commission|transfer
            Route::get('users/accounts/logs','AccountLogController@getAccountLog');

            //Params: key = winmoney|commission|transfer
            Route::get('users/accounts/logs/{id}','AccountLogController@getAccountLogDetail');
        });

        Route::group(['namespace' => 'Promotion'],function(){
            Route::get('promotion','PromotionController@getPromotionInfo');
        });
        
        // Route::get('get-game-config', 'API\LottoConfigController@getGameConfig');
    });
	
});

Route::group(['namespace' => 'API'],function(){
    Route::group(['namespace' => 'Cycle'],function(){
        Route::get('all-cycles','CycleController@getAllCyclesInfo');
    });
});