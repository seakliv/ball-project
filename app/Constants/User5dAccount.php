<?php
namespace App\Constants;

class User5dAccount
{
    const CASH_ACCOUNT = 1;
    const BANK_ACCOUNT = 2;
    const THIRD_PARTY_ACCOUNT = 3;
    const PROFIT_ACCOUNT = 4;
    const BONUS_ACCOUNT = 5;
}