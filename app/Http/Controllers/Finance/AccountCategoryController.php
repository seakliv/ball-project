<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SystemAccount;
use App\Model\SystemAccountType;
use App\Model\SystemAccountCategory;
use DB;


class AccountCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = SystemAccountCategory::with('type')->orderBy('sort','ASC')->paginate(15);
        return view('finance.account.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = SystemAccountType::all()->pluck('name','id');
        return view('finance.account.category.add-update',compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:ball_system_account_categories,name',
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('accounts.categories.index')->withSuccess('You have just added an account class successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = SystemAccountCategory::findOrFail($id);
        $types = SystemAccountType::all()->pluck('name','id');
        return view('finance.account.category.add-update',compact('types','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|unique:ball_system_account_categories,name,'.$id,
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('accounts.categories.index')->withSuccess('You have just updated an account class successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function saveToDB($data, $id=null){
        $category = isset($id) ? SystemAccountCategory::findOrFail($id) : new SystemAccountCategory;
        DB::beginTransaction();
        try{
            if(!$category) return redirect()->back()->withError('There is no record found!');
            $category->fill($data);
            $category->save();
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $category;
    }
}
 