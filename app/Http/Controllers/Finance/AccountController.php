<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SystemAccount;
use App\Model\SystemAccountType;
use App\Model\SystemAccountCategory;
use DB;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $accounts = SystemAccount::with('type','category')->orderBy('sort','ASC')->paginate(15);
        return view('finance.account.index',compact('accounts'))->withError('You have just updated an account successfully!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = SystemAccountType::all()->pluck('name','id');
        $categories = SystemAccountCategory::all()->pluck('name','id');
        return view('finance.account.add-update',compact('types','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request,[
            'title' => 'required|unique:ball_system_accounts,title',
            'name' => 'required',
            'number' => 'required'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('accounts.index')->withSuccess('You have just added an account successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = SystemAccount::findOrFail($id);
        $types = SystemAccountType::all()->pluck('name','id');
        $categories = SystemAccountCategory::all()->pluck('name','id');
        return view('finance.account.add-update',compact('types','categories','account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|unique:ball_system_accounts,title,'.$id,
            'name' => 'required',
            'number' => 'required'
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('accounts.index')->withSuccess('You have just updated an account successfully!');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function saveToDB($data, $id=null){
        $data['state'] = isset($data['state']) ? 1 : 0;
        $account = isset($id) ? SystemAccount::findOrFail($id) : new SystemAccount;
        DB::beginTransaction();
        try{
            if(!$account) return redirect()->back()->withError('There is no record found!');
            $account->fill($data);
            $account->save();
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $account;
    }
}
