<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SystemAccount;
use App\Model\SystemAccountLog;
use DB;
use App\Model\Manager;
use App\Model\ManagerLog;
use App\Constants\Account;

class AccountTransferController extends Controller
{
    public function edit($id)
    {
        $account = SystemAccount::findOrFail($id);
        $accounts = SystemAccount::select('title','name','id')->where('id','!=',$id)->orderBy('sort','ASC')->get()->keyBy('id'); 
        foreach($accounts as $id => $ac){
            $accounts[$id] = $ac->title.' ('.$ac->name.')';
        }
        return view('finance.account.transfer.add-update',compact('account','accounts'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'amount' => 'required'
        ]);
        $abstract = 'LANG_LABEL_TRANS';
        $data = $request->all();
        $data['amount'] = stringToDouble($data['amount']);
        if($data['amount'] == 0) return back()->withInput()->withErrors(['amount' => 'Please enther the amount.']);

        DB::beginTransaction();
        try{
            if(!Manager::checkPayPassword($data['pay_password'])) 
                return back()->withInput()->withErrors(['pay_password' =>'Your pay password does not match!']);

            $account = SystemAccount::find($id);
            if(!$account) 
                return redirect()->back()->withInput()->withError('Unknown Account!');
            
            $toAccount = SystemAccount::find($data['to_account_id']);
            if(!$toAccount) 
                return redirect()->back()->withInput()->withErrors(['to_account_id' => 'Target account unknown!']);

            if($account->balance < $data['amount'])
                return back()->withInput()->withErrors(['amount' =>'You amount is larger than current avaiable balance!']);
                
            $account->decrement('balance',$data['amount']);
            $toAccount->increment('balance',$data['amount']);
            
            $sys_log_out = SystemAccountLog::create([
                'amount' => $data['amount'],
                'balance' => $account->balance,
                'abstract' => $abstract,
                'user_abstract' => '',
                'log_number' => SystemAccountLog::generateLogNumber(Account::LOG_TYPE_OUT),
                'log_type' => Account::LOG_TYPE_OUT,
                'to_type' => Account::SYSTEM_ACCOUNT_TYPE,
                'account_id' => $id,
                'to_account_id' => $toAccount->id,
                'manager_id' => auth()->id()
            ]);
            $sys_log_in = SystemAccountLog::create([
                'amount' => $data['amount'],
                'balance' => $toAccount->balance,
                'abstract' => $abstract,
                'user_abstract' => '',
                'log_number' => SystemAccountLog::generateLogNumber(Account::LOG_TYPE_IN),
                'log_type' => Account::LOG_TYPE_IN,
                'to_type' => Account::SYSTEM_ACCOUNT_TYPE,
                'account_id' => $toAccount->id,
                'to_account_id' => $id,
                'manager_id' => auth()->id()
            ]);
            
            $manager_id = auth()->id();
            $arr_log = [ 
                'action' => 'Transfer',
                'amount' => $data['amount'],
                'out_account_log_id' => $sys_log_out->id,
                'in_account_log_id'  => $sys_log_in->id
            ];
        
            ManagerLog::createManagerLog($manager_id,$arr_log);

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withInput()->withError('There was an error during operation!');
        }

        return redirect()->route('accounts.index')->withSuccess('You have just transferred amount of '.$request->amount.'!');
    }

}
