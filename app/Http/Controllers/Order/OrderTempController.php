<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\OrderTemp;
use App\Model\Order;
use App\Model\Cycle;
use App\User;
use DB;
use App\Model\GameType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class OrderTempController extends Controller
{
    public function index(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $s = microtime(true);
        $showTicketDetail = false;
        $data = [];
        $breadcrumbs = "";
        $table = OrderTemp::getModel()->getTable();
        if(!request()->user_id){ 
            $d = $this->structureSummaryData($table, $request);
            $total = isset($d['total']) ? $d['total'] : null; 
            $users = isset($d['data']) && count($d['data']) > 0 ? $d['data'] : null;
            $hasDirectUser = $d['hasDirectUser'];
            $parentHasOrder = $d['parentHasOrder'];
        }else{
            $d = $this->structureTicketDataForEachUser($table, $request);
            $total = isset($d['total']) ? $d['total'] : null;
            $users = isset($d['data'][0]) ? $d['data'][0] : null;
            $showTicketDetail = true;
            $hasDirectUser = $d['hasDirectUser'];
            $parentHasOrder = $d['parentHasOrder'];
        } 
        
        // Breadcrumbs
        $topLevelUsers = $this->getBreadcrumbList($request);
        
        return view('order.temp.index',compact('users','total','topLevelUsers','showTicketDetail','hasDirectUser','parentHasOrder'));
    }

    public function downloadExcel(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $table = 'order_temps'; 
        $historyReport = false;
        $request = request();  
        $data = $this->structureTicketDataForEachUser($table,$request);
        $path = $this->writeExcel($data);
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function writeExcel($data, $isHistoryList = false){
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Sale Report');
        $spreadsheet = $this->writeDetailWorksheet($spreadsheet, $sheet, $data['data'], $isHistoryList);
        
        $writer = new Xlsx($spreadsheet);
        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
        $filename = "Ball_Sale_Report(".date('Y-m-d').").xlsx";
        if(request()->cycle_id){
            $cycle = Cycle::whereId(request()->cycle_id)->first();
            if($cycle) $filename = "Ball_Sale_Report(".$cycle->cycle_sn.").xlsx";
        }
        $writer->save(public_path('backup').'/'.$filename);
        $path = asset('backup/'.$filename);
        return $path;
    }

    public function writeDetailWorksheet($spreadsheet, $sheet, $data, $isHistoryList){
        $columnNames = $isHistoryList 
                        ? ['Account No','User','Ticket','Post Time', 'Order Bet','Bet Amount (R)','Ticket Amount (R)', 'Ticket Win Amount (R)']
                        : ['Account No','User','Ticket','Post Time', 'Order Bet','Bet Amount (R)','Ticket Amount (R)'];
        
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        $i = 2;
        foreach( $data as $l => $user){
            $sheet->setCellValue('A'.$i, $user['accountno']);
            $sheet->setCellValue('B'.$i, $user['username']);
            $j = 0; 
            foreach($user['ticket'] as $key => $ticket){
                $sheet->setCellValueExplicit('C'.($i + $j), $ticket['ticket'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValue('D'.($i + $j), $ticket['created_at']);
                $sheet->setCellValue('G'.($i + $j), preg_replace("/([^0-9\\.])/i", "", $ticket['ticketAmountR']));
                if($isHistoryList)
                    $sheet->setCellValue('H'.($i + $j), preg_replace("/([^0-9\\.])/i", "", $ticket['ticketWinAmountR']));
                foreach($ticket['orders'] as $index => $o){
                    $sheet->setCellValueExplicit('E'.($i + $j + $index), implode(", ",[$o['no1'],$o['no2'],$o['no3'],$o['no4'],$o['no5'],$o['no6']]), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValue('F'.($i + $j + $index), preg_replace("/([^0-9\\.])/i", "", $o['amountR']));
                }
                $j+=count($ticket['orders']);
            }
            $i+=$j;
        }
        
        return $spreadsheet;
    }

    public function writeHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }

    public function getBreadcrumbList($request){
        $arrayId = [];
        $topLevelUsers = [];
        $thisUser = request()->parent_id == 0 ? User::find(request()->user_id) : User::find(request()->parent_id);
        if($thisUser){
            for($i = 1; $i < $thisUser->level; $i++){
                $arrayId[] = $thisUser->{'l'.$i.'_id'};
            }
            $topLevelUsers = User::whereIn('id', $arrayId)->orderBy('level','ASC')->get()->toArray();
            $topLevelUsers[] = $thisUser->toArray();
        }
        return $topLevelUsers;
    }

    public function structureSummaryData($table, $request){ 
        $data = [];
        $total = [];
        $lv = request()->level == 8 ? request()->level : request()->level + 1;
        $p = request()->parent_id ?? null;
        $arrUserIds = [];
        $exchangeRate = exchangeRateFromRielToDollar(); 
        $hasDirectUser = false;
        $parentHasOrder = false;
        
        // Get List of users for current orders
        $users = $this->getActiveUsers($lv,$p);
        // End Get List of users
        
        // Get Orders Group By Level
        $ordersGroupByUserLevel = $this->getOrdersGroupByUserLevel($table,$lv,$p,$users);
        // End query on conditional filter
        
        $totalOrderAmount = 0;
        $totalMemberAmount = 0; 
        $totalAmount = 0;
        $totalRebate = 0;
        $totalWinAmount = 0;
        $totalProfitAmount = 0;
        
        foreach($ordersGroupByUserLevel as $id => $orders){
            $ordersByChildren = $orders->groupBy('l'.(request()->level+1).'_id');
            $ordersByUserIds = $orders->groupBy('user_id');
            $memberAmount = $ordersByUserIds->count();
            $orderAmount = 0;
            $amount = 0;
            $rebate = 0;
            $winAmount = 0;
            $userType = 2;
            $hasChildren = false;
            if($id == 0){
                $hasChildren = false;
                foreach($ordersByUserIds as $i => $ordersOfUser){ 
                    if($ordersOfUser->first()->user_type == 2){
                        $parentHasOrder = true;
                        $hasDirectUser = false;
                    }else{
                        $parentHasOrder = false;
                        $hasDirectUser = true;
                    }
                    $memberAmount = 1;
                    $orderAmount = 0;
                    $amount = 0;
                    $rebate = 0;
                    $winAmount = 0;
                    $orderAmount += $ordersOfUser->groupBy('ticket')->count();
                    foreach($ordersOfUser as $order){
                        $amount += $order->amount;
                        $winAmount += $order->win_amount;
                        for($i=$lv;$i<=8;$i++){
                            $rebate += $order->{'l'.$i.'_rebate'};
                        }
                    }
                    $data[] = [
                        'user'      => $users[$ordersOfUser->first()->user_id]->toArray(),
                        'memberAmount'   => $memberAmount,
                        'orderAmount'    => $orderAmount,
                        'amountR'    => $amount,
                        'amountD'    => $amount * $exchangeRate,
                        'rebateR' => $rebate,   
                        'rebateD' => $rebate * $exchangeRate,
                        'winAmountR'=> $winAmount,
                        'winAmountD'=> $winAmount * $exchangeRate,
                        'profitAmountR' => $amount - $winAmount - $rebate,
                        'profitAmountD' => ($amount - $winAmount - $rebate)  * $exchangeRate,
                        'userType' => $ordersOfUser->first()->user_type,
                        'hasChildren' => $hasChildren,
                    ]; 
                    
                    $totalOrderAmount += $orderAmount;
                    $totalMemberAmount += $memberAmount;
                    $totalAmount += $amount;
                    $totalRebate += $rebate;
                    $totalWinAmount += $winAmount;
                    $totalProfitAmount += $amount - $winAmount - $rebate;
                }
            }else{
                foreach($ordersByUserIds as $ordersOfUser){
                    $orderAmount += $ordersOfUser->groupBy('ticket')->count();
                    foreach($ordersOfUser as $order){
                        $amount += $order->amount;
                        $winAmount += $order->win_amount;
                        for($i=$lv;$i<=8;$i++){
                            $rebate += $order->{'l'.$i.'_rebate'};
                        }
                        if($hasDirectUser){
                            $userType = 1;
                            $hasChildren = false;
                        }
                    }
                } 
                $hasChildren = isset($ordersByChildren[$id]) ? $ordersByChildren[$id]->count() > 0 : false;
                $data[] = [
                    'user'      => $users[$hasDirectUser ? $user->first()->user_id : $id]->toArray(),
                    'memberAmount'   => $memberAmount,
                    'orderAmount'    => $orderAmount,
                    'amountR'    => $amount,
                    'amountD'    => $amount * $exchangeRate,
                    'rebateR' => $rebate,
                    'rebateD' => $rebate * $exchangeRate,
                    'winAmountR'=> $winAmount,
                    'winAmountD'=> $winAmount * $exchangeRate,
                    'profitAmountR' => $amount - $winAmount - $rebate,
                    'profitAmountD' => ($amount - $winAmount - $rebate)  * $exchangeRate,
                    'userType' => $userType,
                    'hasChildren' => $hasChildren,
                ];

                $totalOrderAmount += $orderAmount;
                $totalMemberAmount += $memberAmount;
                $totalAmount += $amount;
                $totalRebate += $rebate;
                $totalWinAmount += $winAmount;
                $totalProfitAmount += $amount - $winAmount - $rebate;
            }
        }
        $total = [
            'memberAmount' => $totalMemberAmount,
            'orderAmount' => $totalOrderAmount,
            'amountR' => $totalAmount,
            'amountD' => $totalAmount * $exchangeRate,
            'rebateR' => $totalRebate,
            'rebateD' => $totalRebate * $exchangeRate,
            'winAmountR' => $totalWinAmount,
            'winAmountD' => $totalWinAmount * $exchangeRate,
            'profitAmountR' => $totalProfitAmount,
            'profitAmountD' => $totalProfitAmount * $exchangeRate
        ];
        
        return ['data' => $data, 'total' => $total, 'hasDirectUser' => $hasDirectUser, 'parentHasOrder' => $parentHasOrder];
    }

    public function structureTicketDataForEachUser($table, $request){
        $data = [];
        $totalAmount = 0;
        $totalWinAmount = 0;
        $user_id = request()->user_id;
        $exchangeRate = exchangeRateFromRielToDollar();
        $query = $table == Order::getModel()->getTable() ? Order::select()
                                    : OrderTemp::select();
        $ordersByUser = $query
                            ->when(request()->level && request()->parent_id && request()->cycle_id,function($q) use ($request){
                                return $q->where(['l'.request()->level.'_id' => request()->parent_id, 'cycle_id' => request()->cycle_id]);
                            })->when($user_id && request()->cycle_id,function($q) use ($user_id,$request){
                                return $q->where(['user_id' => $user_id, 'cycle_id' => request()->cycle_id]);
                            })
                            ->when(request()->parent_id && request()->level,function($q) use ($request){
                                return $q->where(['l'.request()->level.'_id' => request()->parent_id]);
                            })
                            ->when(request()->cycle_id,function($q) use ($request){
                                return $q->where('cycle_id',request()->cycle_id);
                            })
                            ->when(request()->level && request()->parent_id && request()->begin_date && request()->end_date,function($q) use ($request){
                                return $q->where(['l'.request()->level.'_id' => request()->parent_id])->whereBetween('created_at', [request()->begin_date, request()->end_date]);
                            })
                            ->when($user_id && request()->begin_date && request()->end_date,function($q) use ($user_id,$request){
                                return $q->where(['user_id' => $user_id])->whereBetween('created_at', [request()->begin_date, request()->end_date]);
                            })
                            ->when(request()->begin_date && request()->end_date,function($q) use ($request){
                                return $q->whereBetween('created_at', [request()->begin_date, request()->end_date]);
                            })
                            ->when(request()->user_type, function($q) use ($request){ 
                                return $q->where('user_type',request()->user_type);
                            })
                            ->when(isset(auth()->user()->roles[0]) && auth()->user()->roles[0]->id != 1, function($q){
                                return $q->where('id','!=',45);
                            })
                            ->with('user','cycle')
                            ->orderBy('created_at')
                            ->get(); 
                            
        $totalWinAmount = 0;
        $totalAmount = 0;
        foreach($ordersByUser->groupBy('user_id') as $userOrders){ 
            $t = [];
            foreach($userOrders->groupBy('ticket') as $no => $orders){ 
                $o = [];
                foreach($orders as $order){
                    $o[] = [
                        'amountR' => $order->amount,
                        'amountD' => $order->amount * $exchangeRate,
                        'winAmountR' =>$order->win_amount,
                        'winAmountD' => $order->win_amount  * $exchangeRate,
                        'prize' => $order->win_prize,
                        'no1' => $order->no1,
                        'no2' => $order->no2,
                        'no3' => $order->no3,
                        'no4' => $order->no4,
                        'no5' => $order->no5,
                        'no6' => $order->no6
                    ];
                }
                $t[] = [
                    'cycle'     => $orders[0]->cycle->cycle_sn,
                    'ticket'    => $orders[0]->ticket,
                    'created_at'=> date('Y-m-d H:i A', strtotime($orders[0]->created_at)),
                    'ticketAmountR' => $orders->sum('amount'),
                    'ticketAmountD' => $orders->sum('amount') * $exchangeRate,
                    'ticketWinAmountR'=> $orders->sum('win_amount'),
                    'ticketWinAmountD'=> $orders->sum('win_amount') * $exchangeRate,
                    'orders'    => $o
                ];
            }
            $data[] = [
                'username'  => isset($userOrders[0]) ? $userOrders[0]->user->username : '',
                'accountno' => isset($userOrders[0]) ? $userOrders[0]->user->account_number : '',
                'totalAmountR' => $userOrders->sum('amount'),
                'totalAmountD' => $userOrders->sum('amount') * $exchangeRate,
                'totalWinAmountR' => $userOrders->sum('win_amount'),
                'totalWinAmountD' => $userOrders->sum('win_amount') * $exchangeRate,
                'orders_no' => $userOrders->count(),
                'ticket'    => $t,
            ]; 
            $totalWinAmount += $userOrders->sum('win_amount');
            $totalAmount += $userOrders->sum('amount');
        }   
        $total = [
            'winAmountR' => $totalWinAmount,
            'winAmountD' => $totalWinAmount * $exchangeRate,
            'amountR' => $totalAmount,
            'amountD' => $totalAmount * $exchangeRate
        ];
        
        return ['data' => $data, 'total' => $total, 'hasDirectUser' => false,'parentHasOrder' => false];
    }

    private function getActiveUsers($lv,$p){
        $users = User::select('id','level','username','account_number')
                ->when($lv,function($q) use ($lv,$p){
                    $q->where(['level' => $lv]);
                })
                ->when($p,function($q) use ($p){
                    $q->where(['parent_id' => $p]);
                })
                ->orWhere(function($q) use ($p){
                    $q->where(['id' => $p]);
                })
                ->orWhere(function($q){
                    $q->where(['parent_id' => 0, 'com_direct' => 1]);
                })
                ->when(isset(auth()->user()->roles[0]) && auth()->user()->roles[0]->id != 1, function($q){
                    return $q->where('id','!=',45);
                })
                ->get();

        if($users->count() <= 0){
            $lv = request()->level;
            $users = User::where(['level' => $lv, 'id' => $p])->get();
        } 
        return $users->keyBy('id');
    }

    private function getOrdersGroupByUserLevel($table,$lv,$p,$users){ 
        $game = GameType::whereCode('L101')->first();
        $arrUserIds = !request()->level && !request()->parent_id ? $users->pluck('id')->merge(0) : $users->pluck('id');
        $query = $table == Order::getModel()->getTable() 
                                ? Order::select('ticket','user_id','win_amount','amount','l'.$lv.'_id','l'.($lv == 8 ? $lv : $lv+1).'_id','l1_rebate', 'l2_rebate','l3_rebate','l4_rebate','l5_rebate','l6_rebate','l7_rebate','l8_rebate','user_type','parent_id','state','cycle_id','created_at')
                                : OrderTemp::select('ticket','user_id','win_amount','amount','l'.$lv.'_id','l'.($lv == 8 ? $lv : $lv+1 ).'_id','l1_rebate', 'l2_rebate','l3_rebate','l4_rebate','l5_rebate','l6_rebate','l7_rebate','l8_rebate', 'user_type','parent_id','state','cycle_id','created_at');
        
        $query = $query->when($game, function($q) use ($game){
                    return $q->where('game_type_id',$game->id); 
                })
                ->when(request()->cycle_id, function($q){
                    return $q->where(['cycle_id' => request()->cycle_id]);
                })
                ->when(request()->begin_date && request()->end_date, function($q){ 
                    return $q->whereBetween('created_at',[request()->begin_date,request()->end_date]);
                })
                ->where(['state' => 1, 'is_test' => 0])
                ->whereIn('l'.$lv.'_id',$arrUserIds)
                ->orWhere(function($q) use ($p){
                    $q->where(['user_id' => $p])
                    ->when(request()->cycle_id,function($qq) {
                        $qq->whereCycleId(request()->cycle_id);
                    })
                    ->when(request()->begin_date && request()->end_date, function($qq){
                        $qq->whereBetween('created_at',[request()->begin_date, request()->end_date]);
                    });
                })
                ->orderBy('user_type','DESC')->orderBy('l'.$lv.'_id','ASC')
                ->get()
                ->groupBy('l'.$lv.'_id','user_id');

        return $query;
    }

    public function ajaxGetOrderDetail($ticket){
        $success = false;
        $data = [];
        $orders = OrderTemp::with('user')->whereTicket($ticket)->get();
        if($orders && $orders->count() > 0){
            $data = [
                'ticket' => $orders[0]->ticket,
                'betAmount' => currencyFormat($orders->sum('amount')),
                'winAmount' => currencyFormat($orders->sum('win_amount')),
                'created_at'=> date('Y-m-d H:i A', strtotime($orders[0]->created_at)),
            ];

            if($orders[0]->user->level != 0){
                for($i=1;$i<$orders[0]->user->level + 1;$i++){ 
                    $data['rebates'][] = [
                        'levelTitle' => getUserLevelTitle($i),
                        'username' => $orders[0]->parent($i)->username,
                        'rebate' => currencyFormat($orders->sum('l'.$i.'_rebate'))
                    ];
                }
            }else{
                $data['rebates'][] = [
                    'levelTitle' => "Direct User",
                    'username' => $orders[0]->user->username,
                    'rebate' => currencyFormat(0)
                ];
            }
            $success = true;
        }
        return response()->json(['success' => $success, 'data' => $data]);
    }
}
