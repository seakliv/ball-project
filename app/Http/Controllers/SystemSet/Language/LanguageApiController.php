<?php

namespace App\Http\Controllers\SystemSet\Language;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\LanguageApi;
use Batch;

class LanguageApiController extends Controller
{
    public function index()
    {
        $languages = LanguageApi::all();
        return view('system-set.language.index',compact('languages'));
    }
    public function store(Request $request)
    {
        $data = [];
        foreach($request->language as $id => $row){
            $data[] = [
                'id' => $id,
                'chinese' => $row['chinese'],
                'english' => $row['english'],
                'khmer' => $row['khmer'],
            ];
        }
        Batch::update(new LanguageApi, $data, 'id');
        return redirect()->back()->withSuccess('You have just updated app language successfully!');
    }
}
