<?php

namespace App\Http\Controllers\SystemSet\Location;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Province;
use DB;

class DistrictController extends Controller
{
    public function index()
    {
        $districts = District::with('province')
                                ->when(request()->province_id && request()->province_id != 0,function($q){
                                    return $q->whereProvinceId(request()->province_id);
                                })
                                ->orderBy('province_id','ASC')
                                ->orderBy('name','ASC')->paginate(40);
        $provinces = ['0' => 'All'] + Province::orderBy('name','ASC')->pluck('name','id')->toArray();
        return view('system-set.location.district.index',compact('districts','provinces'));
    }

    public function create()
    {
        $provinces = Province::orderBy('name','ASC')->pluck('name','id')->toArray();
        return view('system-set.location.district.add-update',compact('provinces'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:districts,name'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('system-set.locations.districts.index')->withSuccess('You have just added a district successfully!');    
    }

    public function edit($id)
    {
        $district = District::findOrFail($id);
        $provinces = Province::orderBy('name','ASC')->pluck('name','id')->toArray();
        return view('system-set.location.district.add-update',compact('district','provinces'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|unique:districts,name,'.$id
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('system-set.locations.districts.index')->withSuccess('You have just updated a district successfully!');  
    }

    public function destroy($id)
    {
        $result = false;
        $item = District::findOrFail($id);
        if($item){
            DB::beginTransaction();
            try{
                if($item->delete()){
                    $result = true;
                }
                DB::commit();
            }catch(Exception $exception){
                DB::rollback();
                $result = false;
            }
        }
        return response()->json(['success' => $result]);
    }

    public function getDistrictsByProvinceId(){
        $districts = District::whereProvinceId(request()->id)->pluck('name','id')->toArray();
        return response()->json(['success' => true, 'data' => $districts]);
    }

    public function saveToDB($data, $id=null){
        $district = isset($id) ? District::find($id) : new District;
        DB::beginTransaction();
        try{
            if(!$district) return redirect()->back()->withError('There is no record found!');
            $district->fill($data);
            $district->save();
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $district;
    }
}
