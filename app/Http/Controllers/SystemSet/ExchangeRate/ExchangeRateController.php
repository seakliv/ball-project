<?php

namespace App\Http\Controllers\SystemSet\ExchangeRate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Exchange;
use App\Model\Currency;
use DB;

class ExchangeRateController extends Controller
{
    public function index()
    {
        $exchangeRate = Exchange::with('from','to')->first();
        return view('system-set.exchange-rate.index',compact('exchangeRate'));
    }
    public function store(Request $request)
    {
        $this->saveToDB($request->all());
        return redirect()->route('system-set.exchange-rate.index')->withSuccess('You have just updated exchange rate successfully!');
    }

    public function saveToDB($data){
        $exchangeRate = Exchange::first();
        if($exchangeRate){
            DB::beginTransaction();
            try{
                $exchangeRate->fill($data);
                $exchangeRate->save();
                DB::commit();
            }catch(Exception $ex){
                DB::rollback();
            }
        }
    }
}
