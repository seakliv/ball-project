<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
Use App\Model\OrderTemp;
use App\Model\Order;
use App\User;

class DashboardController extends Controller
{
    public function index()
    {
        $data = [];
        
        // Current Total
        $orderTemps = OrderTemp::all();
        $data['current']['totalMembers'] = $orderTemps->pluck('user_id')->unique()->count();
        $data['current']['totalRevenue'] = $orderTemps->sum('amount');
        $data['current']['totalRebate'] = $orderTemps->sum('l1_rebate') + $orderTemps->sum('l2_rebate') + $orderTemps->sum('l3_rebate') +
                                            $orderTemps->sum('l4_rebate') + $orderTemps->sum('l5_rebate') + $orderTemps->sum('l6_rebate') +
                                            $orderTemps->sum('l7_rebate') + $orderTemps->sum('l8_rebate');

        // By Line Total
        $lines = $orderTemps->groupBy('l1_id');
        $data['line']['pp'] = isset($lines[2563]) ? $lines[2563]->sum('amount') : 0;
        $data['line']['pp_master'] = isset($lines[2730]) ? $lines[2730]->sum('amount') : 0;
        $data['line']['shop'] = (isset($lines[2444]) ? $lines[2444]->sum('amount') : 0)
                                + (isset($lines[2509]) ? $lines[2509]->sum('amount') : 0) 
                                + (isset($lines[2518]) ? $lines[2518]->sum('amount') : 0) ;
        $data['line']['province'] = isset($lines[3]) ? $lines[3]->sum('amount') : 0;
        
        // Monthly
        $orders = Order::whereYear('created_at',date('Y'))->whereMonth('created_at',date('m'))->get();
        $data['monthly']['totalRevenue'] = $orders->sum('amount') + $data['current']['totalRevenue'];
        $data['monthly']['totalProfit'] = $data['monthly']['totalRevenue'] - $orders->sum('win_amount')
                                            - ($orders->sum('l1_rebate') + $orders->sum('l2_rebate') + $orders->sum('l3_rebate') +
                                                $orders->sum('l4_rebate') + $orders->sum('l5_rebate') + $orders->sum('l6_rebate') +
                                                $orders->sum('l7_rebate') + $orders->sum('l8_rebate'))
                                            - $data['current']['totalRebate'];
        
        return view('dashboard.index',compact('data'));
    }
}
