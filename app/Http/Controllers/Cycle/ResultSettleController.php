<?php

namespace App\Http\Controllers\Cycle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\GameType;
use App\Model\Cycle;
use App\Model\OrderTemp;
use App\Model\Order;
use Validator;
use DB;
use Artisan;
use Batch;
use App\User;
use App\Model\UserAccountLog;
use App\Constants\Account;

class ResultSettleController extends Controller
{
    public function setResultForm(){
        $cycle = Cycle::whereHasReleased(0)->orderBy('id','DESC')->first();
        if(!$cycle) return redirect()->route('cycles.set.index')->withError('The cycle could not be found!');
        if($cycle->state) $cycle->update(['state' => 0]);
        return view('cycle.set.result-set',compact('cycle'));
    }
    public function setResult(Request $request){
        $data = $request->all();
        $game = GameType::whereCode('L101')->first();
        $min = $game->min;
        $max = $game->max;
        $validator = Validator::make($request->all(),[
            'no1' => 'required|numeric|min:'.$min.'|max:'.$max,
            'no2' => 'required|numeric|min:'.$min.'|max:'.$max,
            'no3' => 'required|numeric|min:'.$min.'|max:'.$max,
            'no4' => 'required|numeric|min:'.$min.'|max:'.$max,
            'no5' => 'required|numeric|min:'.$min.'|max:'.$max,
            'no6' => 'required|numeric|min:'.$min.'|max:'.$max
        ]);
        if ($validator->fails()) { 
            return back()->withError('Please fill all the lottery number from 01 to 35!');
        }

        // check if input numbers are the same
        $arr = array_unique([$request['no1'],$request['no2'],$request['no3'],$request['no4'],$request['no5'],$request['no6']]);
        if(count($arr) != 6) return back()->withInput()->withError('Please fill all the lottery number with unique number!');

        $cycle = Cycle::whereCycleSn($data['cycle_sn'])->first();
        
        if(!$cycle) return redirect()->route('cycles.set.index')->withError('The cycle could not be found!');

        DB::beginTransaction();
        try{
            $cycle->fill([
                'result_number' => [$data['no1'],$data['no2'],$data['no3'],$data['no4'],$data['no5'],$data['no6']]
            ]);
            $cycle->save();
            DB::commit();
            return redirect()->route('cycles.result.set')->withSuccess('You have just save the lottery result successfully!');
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->route('cycles.result.set')->withError('There was an error during operation!');
        }
    }

    public function settleResultLoading(){
        return view('cycle.set.settle');
    }
    public function settleResult(Request $request){
        $data = $request->all();
        $cycle = Cycle::whereCycleSn($data['cycle_sn'])->first();
        
        if(!$cycle) return redirect()->route('cycles.result.index')->withError('The cycle could not be found!');

        if(!$cycle->result_number || count($cycle->result_number) != 6) return redirect()->back()->withError('Please save the lottery result first!');
        
        return view('cycle.set.settle',compact('cycle'));
    }

    public function ajaxSettleResult(){ 
        $success = false;
        if(request()->ajax()){
            $cycle = Cycle::whereCycleSn(request()->cycle_sn)->first();
            if($cycle && count($cycle->result_number) == 6){
                Artisan::call('settle:result');
                $success = true;
            }else{
                $success = false;
            }
        }
        return response()->json(['success' => $success]);
    }
}
