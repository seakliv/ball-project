<?php

namespace App\Http\Controllers\Cycle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\GameType;
use App\Model\Cycle;
use DB;

class CycleSetController extends Controller
{
    public function index()
    {   
        $game = GameType::whereCode('L101')->first();
        $cycle = Cycle::orderBy('cycle_sn','DESC')->first();
        
        $cycle_sn = $cycle 
                    ? !$cycle->state && $cycle->has_released ? $cycle->cycle_sn + 1: $cycle->cycle_sn 
                    : date('Y').addPrefixStringPad(1,5,'0');
        $prize = $cycle 
                ? !$cycle->state && $cycle->has_released
                    ? $cycle->is_jackpot ? $game->start_prize : $cycle->prize
                    : $cycle->prize
                : $game->start_prize; 
        $prize = currencyFormat($prize * exchangeRateFromRielToDollar());
        $cycle = $cycle && $cycle->has_released ? null : $cycle;
        return view('cycle.set.index',compact('cycle_sn','prize','game','cycle'));
    }

    public function store(Request $request)
    {   
        $cycle = $this->saveToDB($request->all());
        return redirect()->route('cycles.set.index')->withSuccess('You have just updated the cycle successfully!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {     
        $this->validate($request,[
            'cycle_sn' => 'required|unique:ball_cycles,cycle_sn,'.$id
        ]);
        $cycle = $this->saveToDB($request->all(),$id);
        return redirect()->route('cycles.set.index')->withSuccess('You have just updated the cycle successfully!');
    }

    public function saveToDB($data,$id = null){
        
        DB::beginTransaction();
        try{ 
            $cycle = isset($id) ? Cycle::find($id) : new Cycle; 
            if(!$cycle) return redirect()->back()->withError('There is no record found!');

            $lastCycle = Cycle::orderBy('cycle_sn','DESC')->first();
            $game = GameType::whereCode('L101')->first();
            
            $data['stopped_time'] = strtotime($data['stopped_time']);
            $data['result_time'] = strtotime($data['result_time']);
            $data['prize'] = $lastCycle ? !$lastCycle->state && $lastCycle->has_released
                                            ? $lastCycle->is_jackpot ? $game->start_prize : $lastCycle->prize
                                            : $lastCycle->prize
                                    : $game->start_prize;
            $data['prize_5d'] = $game->win_prize_5d;
            $data['prize_4d'] = $game->win_prize_4d;
            $data['prize_3d'] = $game->win_prize_3d;
            $data['game_type_id'] = $game->id;
            $cycle->fill($data);
            $cycle->save();
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $cycle;
    }

}
