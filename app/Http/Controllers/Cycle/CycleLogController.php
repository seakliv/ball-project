<?php

namespace App\Http\Controllers\Cycle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cycle;
use Illuminate\Pagination\LengthAwarePaginator;
class CycleLogController extends Controller
{
    
    public function index()
    {
        $logs = Cycle::where('state',0)->where('has_released',1)->orderBy('id','DESC')->paginate(15);

        return view('cycle.log.index',compact('logs'));
    }

   
    public function create()
    {
        //
    }

 
    public function store(Request $request)
    {
        //
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
