<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use DB;
use App\Model\UserAccount;
use App\Model\UserAccountLog;
use App\User;
use App\Constants\UserBallAccount;
use App\Constants\Account;
class AccountController extends BaseController
{
    private $user;
    private $amount;
    private $key;
    private $toUser;
    private $toUserAccount;

    
    public function getUserAccounts(){
        $user = auth()->user()->load('ballAccounts');
        if(!$user)
            return $this->sendError(api_trans('auth.permission_denied'));

        $accounts = $user->ballAccounts;
        
        $data = [
            'main' => 
                $accounts->where('type_id',UserBallAccount::CASH_ACCOUNT)->first() ? 
                    $accounts->where('type_id',UserBallAccount::CASH_ACCOUNT)->first()->balance : 0,
            'profit' => 
                $accounts->where('type_id',UserBallAccount::PROFIT_ACCOUNT)->first() ? 
                    $accounts->where('type_id',UserBallAccount::PROFIT_ACCOUNT)->first()->balance : 0,
            'bonus' => 
                $accounts->where('type_id',UserBallAccount::BONUS_ACCOUNT)->first() ? 
                    $accounts->where('type_id',UserBallAccount::BONUS_ACCOUNT)->first()->balance : 0,
            'win' =>  $user->win_money_ball,
            'commission' =>  $user->commission_ball + $user->current_commission_ball
        ];
        
        return $this->sendResponse($data, 'OK');
    }

    public function tranferToOtherCashAccount(){
        
        $userAccountNo = request()->to_user_accountno;
        $pwd = request()->pay_password;
        $this->amount = request()->amount;
        $this->key = 'transfer';
        // return $this->sendError(api_trans('auth.permission_denied'));
        
        DB::beginTransaction();
        try{
            $this->user = auth()->user();
            if(!$this->user || !$this->user->state || $this->user->is_shop)
                return $this->sendError(api_trans('auth.permission_denied'));
            
            if(!$this->user->ballCashAccount || $this->user->ballCashAccount->balance < $this->amount)
                return $this->sendError(api_trans('account.over_balance'));
            
            if(!User::checkPayPassword($this->user->id,$pwd)) 
                return $this->sendError(api_trans('account.wrong_pay_password'));
            
            $this->toUser = User::with('ballCashAccount')->where('account_number',$userAccountNo)->first();
            if(!$this->toUser || $this->toUser->id == $this->user->id)
                return $this->sendError(api_trans('account.unknown_target_user'));

            if(!$this->toUser->ballCashAccount)
                $this->toUser->ballCashAccount = UserAccount::createCashAccount($this->toUser->id);

            // Bonus Agent Top up
            $now = time();
            $start_dt = strtotime('2019-06-25 00:00:00');
            $end_dt = strtotime('2019-06-30 23:59:59');
            if(($now >= $start_dt && $now <= $end_dt) 
                && $this->user->level != 8 && $this->toUser->level == 8
                && $this->amount >= 200000){
                $bonus = 0;
                $percentage = 0;
                if(!$this->toUser->ballBonusAccount)
                    $this->toUser->ballBonusAccount = UserAccount::createUserBonusAccount($this->toUser->id);
                
                if($this->amount >= 2000000){
                    $percentage = 0.2;
                }elseif($this->amount >= 800000){
                    $percentage = 0.15;
                }elseif($this->amount >= 400000){
                    $percentage = 0.12;
                }elseif($this->amount >= 200000){
                    $percentage = 0.05;
                }
                $bonus = $this->amount * $percentage;
                if($bonus != 0){
                    $this->toUser->ballBonusAccount->increment('balance',$bonus + $this->amount);
                    UserAccountLog::create([
                        'user_id' => $this->toUser->id,
                        'account_id' => $this->toUser->ballBonusAccount->id,
                        'log_type' => Account::LOG_TYPE_IN,
                        'is_transfer' => 1,
                        'amount' => $bonus,
                        'balance' => $this->toUser->ballBonusAccount->balance,
                        'commission' => 0,
                        'win_money' => 0,
                        'to_type' => Account::SYSTEM_ACCOUNT_TYPE,
                        'to_user_id' => 0,
                        'to_account_id' => 0,
                        'abstract' => 'LANG_LABEL_BONUS_TOP_UP',
                        'manager_id' => 0,
                    ]);
                }
            }else{
                $this->toUser->ballCashAccount->increment('balance',$this->amount);
            }
        
            $this->user->ballCashAccount->decrement('balance',$this->amount);
            $this->createLog();
            pushNotificationToUser(
                api_trans('account.balance_transfer'),
                api_trans('account.receive_msg',['amount' => $this->amount, 'name' => $this->user->username]),
                [(string)$this->toUser->id]
            );
            
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return $this->sendError(api_trans('account.failed_to_transfer'));
        }
        return $this->sendResponse(currencyFormat($this->user->ballCashAccount->balance)." R", 'OK');
    }

    public function tranferToMainBalance(){
        
        $this->key = request()->key;
        $this->amount = (double) request()->amount;
        // return $this->sendError(api_trans('auth.permission_denied'));
        if(!$this->amount && $this->amount <= 0)
            return $this->sendError(api_trans('account.failed_amount_input'));
        
        DB::beginTransaction();
        try{
            $this->user = auth()->user();
            
            if(!$this->user || !$this->user->state || $this->user->is_shop)
                return $this->sendError(api_trans('auth.permission_denied'));

            if(!$this->user->ballCashAccount)
                $this->user->ballCashAccount = UserAccount::createCashAccount($this->user->id);
            
            if($this->key == 'winmoney'){
                if($this->user->win_money_ball < $this->amount)
                    return $this->sendError(api_trans('account.over_balance'));
                
                $this->user->ballCashAccount->increment('balance',$this->amount);
                $this->user->decrement('win_money_ball',$this->amount);
                $this->createLog();
            } 
            elseif($this->key == 'commission'){
                if($this->user->commission_ball < $this->amount)
                    return $this->sendError(api_trans('account.over_balance'));
                
                $this->user->ballCashAccount->increment('balance',$this->amount);
                $this->user->decrement('commission_ball',$this->amount);
                $this->createLog();
            }
            // elseif($this->key == 'profit'){
            //     $this->ballProfitAccount = $this->user->ballProfitAccount;
            //     if(!$this->ballProfitAccount && $this->user->commission_ball < $this->amount)
            //         return $this->sendError(api_trans('account.over_balance'));
                
            //     $this->user->ballCashAccount->increment('balance',$this->amount);
            //     $this->ballProfitAccount->decrement('balance',$this->amount);
            // }
            else{
                return $this->sendError(api_trans('account.unknown_account'));
            }
            
            DB::commit();
        }catch(Exception $ex){
            
            DB::rollback();
            return $this->sendError(api_trans('account.failed_to_transfer'));
        }
        return $this->sendResponse(currencyFormat($this->user->ballCashAccount->balance)." R", 'OK');
    }

    public function createLog(){
        $log = null;
        if($this->key == 'winmoney'){
            UserAccountLog::create([
                'user_id' => $this->user->id,
                'account_id' => $this->user->ballCashAccount->id,
                'log_type' => Account::LOG_TYPE_OWN_ACCOUNT,
                'is_transfer' => 0,
                'amount' => $this->amount,
                'balance' => $this->user->ballCashAccount->balance,
                'commission' => 0,
                'win_money' => $this->user->win_money_ball,
                'to_type' => Account::USER_ACCOUNT_TYPE,
                'to_user_id' => $this->user->id,
                'to_account_id' => $this->user->ballCashAccount->id,
                'abstract' => 'LANG_WIN_MONEY_TO_BALANCE',
                'manager_id' => 0,
                'log_number' => '',
            ]);
        } 
        elseif($this->key == 'commission'){
            UserAccountLog::create([
                'user_id' => $this->user->id,
                'account_id' => $this->user->ballCashAccount->id,
                'log_type' => Account::LOG_TYPE_OWN_ACCOUNT,
                'is_transfer' => 0,
                'amount' => $this->amount,
                'balance' => $this->user->ballCashAccount->balance,
                'commission' => $this->user->commission_ball,
                'win_money' => 0,
                'to_type' => Account::USER_ACCOUNT_TYPE,
                'to_user_id' => $this->user->id,
                'to_account_id' => $this->user->ballCashAccount->id,
                'abstract' => 'LANG_COMMISSION_TO_BALANCE',
                'manager_id' => 0,
                'log_number' => '',
            ]);
        }
        elseif($this->key == 'transfer'){
            UserAccountLog::create([
                'user_id' => $this->user->id,
                'account_id' => $this->user->ballCashAccount->id,
                'log_type' => Account::LOG_TYPE_OUT,
                'is_transfer' => 1,
                'amount' => $this->amount,
                'balance' => $this->user->ballCashAccount->balance,
                'commission' => 0,
                'win_money' => 0,
                'to_type' => Account::USER_ACCOUNT_TYPE,
                'to_user_id' => $this->toUser->id,
                'to_account_id' => $this->toUser->ballCashAccount->id,
                'abstract' => 'LANG_LABEL_TRANS',
                'manager_id' => 0,
                'log_number' => UserAccountLog::generateLogNumber(Account::LOG_TYPE_OUT,$this->user->id),
            ]);
            UserAccountLog::create([
                'user_id' => $this->toUser->id,
                'account_id' => $this->toUser->ballCashAccount->id,
                'log_type' => Account::LOG_TYPE_IN,
                'is_transfer' => 1,
                'amount' => $this->amount,
                'balance' => $this->toUser->ballCashAccount->balance,
                'commission' => 0,
                'win_money' => 0,
                'to_type' => Account::USER_ACCOUNT_TYPE,
                'to_user_id' => $this->user->id,
                'to_account_id' => $this->user->ballCashAccount->id,
                'abstract' => 'LANG_LABEL_TRANS',
                'manager_id' => 0,
                'log_number' => UserAccountLog::generateLogNumber(Account::LOG_TYPE_IN,$this->toUser->id),
            ]);
        }
        else{
            return $this->sendError(api_trans('account.unknown_account'));
        }
        
    }
}
