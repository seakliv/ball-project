<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\User;
use DB;
use App\Constants\UserBallAccount;

class UserController extends BaseController
{

    public function getUserByAccount(){
        $accountno = request()->accountno;
        $user = User::where('account_number',$accountno)->where('id','!=',auth()->id())->first();
        if(!$user)
            return $this->sendError(api_trans('account.unknown_target_user'),404);
        
        return $this->sendResponse($user->username, 'OK');
    }
}
