<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\App;

class BaseController extends Controller
{
    public function sendResponse($result, $message, $code = 200)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message
        ];
        return response()->json($response, $code, [], JSON_UNESCAPED_UNICODE)
                        ->withHeaders([
                            'Version' => App::VERSION
                        ]);
    }

    public function sendError($error, $code = 200)
    {
        $response = [
            'success' => false,
            'data' => null,
            'code' => 403,
            'message' => $error,
        ];
        return response()->json($response, $code, [], JSON_UNESCAPED_UNICODE)
                        ->withHeaders([
                            'Version' => App::VERSION
                        ]);
    }
}
