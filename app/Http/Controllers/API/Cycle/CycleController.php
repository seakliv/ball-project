<?php

namespace App\Http\Controllers\API\Cycle;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Model\Cycle;
use App\Model\GameType;
use DB;

class CycleController extends BaseController
{
    public function getActiveCycle(){
        $cycle = Cycle::whereStateAndHasReleased(1,0)->first();
        if(!$cycle){
            return  $this->sendError(api_trans('cycle.no_cycle'));
        }

        $rateRielToDollar = exchangeRateFromRielToDollar();
        $cycle['prize'] = currencyFormat($cycle['prize'] * $rateRielToDollar)." $";
        $cycle['prize_5d'] = currencyFormat($cycle['prize_5d'] * $rateRielToDollar)." $";
        $cycle['prize_4d'] = currencyFormat($cycle['prize_4d'] * $rateRielToDollar)." $";
        $cycle['prize_3d'] = currencyFormat($cycle['prize_3d'] * $rateRielToDollar)." $";
        $cycle['stopped_time'] = date('Y-m-d H:i A',$cycle['stopped_time']);
        $cycle['result_time'] = date('Y-m-d H:i A',$cycle['result_time']);

        return $this->sendResponse($cycle, 'OK');
    }

    public function getCycleById($cycle_id){
        
        $cycle = $cycle_id ? Cycle::find($cycle_id)->toArray() : null;
        $rateRielToDollar = exchangeRateFromRielToDollar();

        if(!$cycle){
            return  $this->sendError(api_trans('cycle.no_cycle'));
        }

        $cycle['prize'] = currencyFormat($cycle['prize'] * $rateRielToDollar)." $";
        $cycle['prize_5d'] = currencyFormat($cycle['prize_5d'] * $rateRielToDollar)." $";
        $cycle['prize_4d'] = currencyFormat($cycle['prize_4d'] * $rateRielToDollar)." $";
        $cycle['prize_3d'] = currencyFormat($cycle['prize_3d'] * $rateRielToDollar)." $";
        $cycle['result_number'] = $cycle['result_number'] ? json_encode($cycle['result_number']) : null;
        $cycle['stopped_time'] = date('Y-m-d H:i A',$cycle['stopped_time']);
        $cycle['result_time'] = date('Y-m-d H:i A',$cycle['result_time']);
        
        return $this->sendResponse($cycle, 'OK');
    }

    public function getfinishedCycle(){
        $cycles = Cycle::whereStateAndHasReleased(0,1)->orderBy('id','DESC')->paginate(15)->toArray();
        $rateRielToDollar = exchangeRateFromRielToDollar();
        $cycles['cycles'] = $cycles['data'];
        unset($cycles['data']);
        if(!$cycles){
            return  $this->sendError(api_trans('cycle.no_cycle'));
        }

        foreach($cycles['cycles'] as $i => $cycle){
            $cycles['cycles'][$i]['prize'] = currencyFormat($cycle['prize'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_5d'] = currencyFormat($cycle['prize_5d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_4d'] = currencyFormat($cycle['prize_4d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_3d'] = currencyFormat($cycle['prize_3d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['result_number'] = $cycle['result_number'] ? json_encode($cycle['result_number']) : null;
            $cycles['cycles'][$i]['result_time'] = date('Y-m-d H:i A',$cycle['result_time']);
            $cycles['cycles'][$i]['stopped_time'] = date('Y-m-d H:i A',$cycle['stopped_time']);
        }
        
        return $this->sendResponse($cycles, 'OK');
    }

    public function getAllInvolvedCycles(){
        $userId = auth()->id();
        $page = request()->get('page',1);
        $currentCycle = [];

        $cycles = Cycle::orderBy('id','DESC')
                        ->whereHas('orders',function($q) use ($userId){
                            return $q->where('user_id',$userId);
                        })
                        ->paginate(15)
                        ->toArray();
        $rateRielToDollar = exchangeRateFromRielToDollar();
        $cycles['cycles'] = $cycles['data'];
        unset($cycles['data']);
        if(!$cycles){
            return  $this->sendError(api_trans('cycle.no_cycle'));
        }

        foreach($cycles['cycles'] as $i => $cycle){
            $cycles['cycles'][$i]['prize'] = currencyFormat($cycle['prize'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_5d'] = currencyFormat($cycle['prize_5d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_4d'] = currencyFormat($cycle['prize_4d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_3d'] = currencyFormat($cycle['prize_3d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['result_number'] = $cycle['result_number'] ? json_encode($cycle['result_number']) : null;
            $cycles['cycles'][$i]['result_time'] = date('Y-m-d H:i A',$cycle['result_time']);
            $cycles['cycles'][$i]['stopped_time'] = date('Y-m-d H:i A',$cycle['stopped_time']);
        }


        if($page == 1){
            $currentCycle = Cycle::whereHasReleased(0)->whereHas('order_temps',function($query) use ($userId){
                                $query->where('user_id',$userId);
                            })->first();
            if($currentCycle){
                $currentCycle = $currentCycle->toArray();
                $currentCycle['prize'] = currencyFormat($currentCycle['prize'] * $rateRielToDollar)." $";
                $currentCycle['prize_5d'] = currencyFormat($currentCycle['prize_5d'] * $rateRielToDollar)." $";
                $currentCycle['prize_4d'] = currencyFormat($currentCycle['prize_4d'] * $rateRielToDollar)." $";
                $currentCycle['prize_3d'] = currencyFormat($currentCycle['prize_3d'] * $rateRielToDollar)." $";
                $currentCycle['result_number'] = $currentCycle['result_number'] ? json_encode($currentCycle['result_number']) : null;
                $currentCycle['result_time'] = date('Y-m-d H:i A',$currentCycle['result_time']);
                $currentCycle['stopped_time'] = date('Y-m-d H:i A',$currentCycle['stopped_time']);
            }
            $cycles['cycles'] = array_merge([$currentCycle],$cycles['cycles']);
        }
        
        return $this->sendResponse($cycles, 'OK');
    }

    public function getAllCycles(){
        $currentCycle = [];

        $cycles = Cycle::orderBy('id','DESC')
                        ->paginate(15)
                        ->toArray();
        $rateRielToDollar = exchangeRateFromRielToDollar();
        $cycles['cycles'] = $cycles['data'];
        unset($cycles['data']);
        if(!$cycles){
            return  $this->sendError(api_trans('cycle.no_cycle'));
        }

        foreach($cycles['cycles'] as $i => $cycle){
            $cycles['cycles'][$i]['prize'] = currencyFormat($cycle['prize'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_5d'] = currencyFormat($cycle['prize_5d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_4d'] = currencyFormat($cycle['prize_4d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_3d'] = currencyFormat($cycle['prize_3d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['result_number'] = $cycle['result_number'] ? json_encode($cycle['result_number']) : null;
            $cycles['cycles'][$i]['result_time'] = date('Y-m-d H:i A',$cycle['result_time']);
            $cycles['cycles'][$i]['stopped_time'] = date('Y-m-d H:i A',$cycle['stopped_time']);
        }
        
        return $this->sendResponse($cycles, 'OK');
    }
    

    public function getTimeRemain(){
        $cycle = Cycle::orderBy('id','DESC')->first();
        $remain = 0;
        if($cycle && $cycle->stopped_time > strtotime('NOW')){
            $now = strtotime('NOW');
            $endtime = $cycle->stopped_time;
            $remain = $endtime - $now;
        }
        
        $balance = auth()->user()->ballCashAccount->balance;
        $data = [
            'remain' => $remain,
            'balance' => currencyFormat($balance)." R"
        ];
        return $this->sendResponse(json_encode($data), 'OK');
    }

    public function getAllCyclesInfo(){
        
        $cycles = Cycle::whereStateAndHasReleased(0,1)->orderBy('id','DESC')->paginate(10)->toArray();
        $rateRielToDollar = exchangeRateFromRielToDollar();
        $cycles['cycles'] = $cycles['data'];
        unset($cycles['data']);
        if(!$cycles){
            return  $this->sendError(api_trans('cycle.no_cycle'));
        }

        foreach($cycles['cycles'] as $i => $cycle){
            $cycles['cycles'][$i]['prize'] = currencyFormat($cycle['prize'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_5d'] = currencyFormat($cycle['prize_5d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_4d'] = currencyFormat($cycle['prize_4d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_3d'] = currencyFormat($cycle['prize_3d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['result_number'] = $cycle['result_number'] ? json_encode($cycle['result_number']) : null;
            $cycles['cycles'][$i]['result_time'] = date('Y-m-d H:i A',$cycle['result_time']);
            $cycles['cycles'][$i]['stopped_time'] = date('Y-m-d H:i A',$cycle['stopped_time']);
        }


        $activeCycle = Cycle::whereStateAndHasReleased(1,0)->first()->toArray();
        $activeCycle['prize'] = currencyFormat($cycle['prize'] * $rateRielToDollar)." $";
        $activeCycle['prize_5d'] = currencyFormat($cycle['prize_5d'] * $rateRielToDollar)." $";
        $activeCycle['prize_4d'] = currencyFormat($cycle['prize_4d'] * $rateRielToDollar)." $";
        $activeCycle['prize_3d'] = currencyFormat($cycle['prize_3d'] * $rateRielToDollar)." $";
        $activeCycle['result_number'] = $cycle['result_number'] ? json_encode($cycle['result_number']) : null;
        $activeCycle['result_time'] = date('Y-m-d H:i A',$cycle['result_time']);
        $activeCycle['stopped_time'] = date('Y-m-d H:i A',$cycle['stopped_time']);
        
        return response()->json(['active' => $activeCycle, 'finished' => $cycles]);
    }
}
