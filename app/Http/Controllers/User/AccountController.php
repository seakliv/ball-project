<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\UserAccount;
use DB;

class AccountController extends Controller
{
    public function show($id)
    {
        DB::beginTransaction();
        try{
            $user = User::with('ballAccounts')->whereId($id)->first();
            if(!$user)
                return redirect()->back()->withInput()->withError('Unknown User!');
            $accounts = $user->ballAccounts;
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withInput()->withError('There was an error during operation!');
        }
        return view('user.account.index',compact('user','accounts'));
    }
}
