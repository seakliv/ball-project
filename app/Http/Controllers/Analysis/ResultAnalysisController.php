<?php

namespace App\Http\Controllers\Analysis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cycle;
use App\Model\GameType;
use Validator;
use App\Model\OrderTemp;

class ResultAnalysisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $game = GameType::whereCode('L101')->first();
        $min = $game->min;
        $max = $game->max;
        $validator = Validator::make($request->all(),[
            'no1' => 'numeric|min:'.$min.'|max:'.$max,
            'no2' => 'numeric|min:'.$min.'|max:'.$max,
            'no3' => 'numeric|min:'.$min.'|max:'.$max,
            'no4' => 'numeric|min:'.$min.'|max:'.$max,
            'no5' => 'numeric|min:'.$min.'|max:'.$max,
            'no6' => 'numeric|min:'.$min.'|max:'.$max
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withError('Please enter the correct number!');
        }
        
        $orders = OrderTemp::orderBy('created_at','ASC');
        $result = [$request->no1,$request->no2,$request->no3,$request->no4,$request->no5,$request->no6];
        $result3N = [];
        $result4N = [];
        $result5N = [];
        $result6N = [];
        $data = [];
        if($request->no1 && $request->no2 && $request->no3 && $request->no4 && $request->no5 && $request->no6){
            foreach($orders->get() as $order){
                $bet = [$order->no1, $order->no2, $order->no3, $order->no4, $order->no5, $order->no6];
                $win_number = array_values(array_intersect($bet, $result));
                if(count($win_number) == 6){
                    $result6N[] =$order;
                }elseif(count($win_number) == 5){
                    $result5N[] =$order;
                }elseif(count($win_number) == 4){
                    $result4N[] =$order;
                }elseif(count($win_number) == 3){
                    $result3N[] =$order;
                }
            }
            $data = [
                '6D' => $result6N,
                '5D' => $result5N,
                '4D' => $result4N,
                '3D' => $result3N,
            ];
        }
        $orders = $orders->paginate(20)->onEachSide(1);
        
        return view('analyse.index',compact('game','orders','data'));
    }
}
