<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserAccountType extends Model
{
    protected $table = "ball_user_account_types";
    protected $fillable = [
        'id','name'
    ];
}
