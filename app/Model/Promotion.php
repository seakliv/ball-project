<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = "ball_promotions";
    protected $fillable = [
        'id',
        'name',
        'from_date',
        'to_date',
        'target_order',
        'bonus_order',
        'running_text'
    ];
}
