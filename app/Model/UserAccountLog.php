<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Manager;
use App\User;
use App\Model\UserAccount;

class UserAccountLog extends Model
{
    protected $table = "ball_user_account_logs";
    protected $fillable = [
        'id',
        'log_number',
        'amount',
        'balance',
        'commission',
        'win_money',
        'is_transfer',
        'log_type',
        'abstract',
        'to_type',

        'user_id',
        'account_id',
        'to_user_id',
        'to_account_id',
        'manager_id',
    ];

    public function account(){
        return $this->belongsTo(UserAccount::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function toUser(){
        return $this->belongsTo(User::class,'to_user_id','id');
    }
    public function manager(){
        return $this->belongsTo(Manager::class,'manager_id','id');
    }
    
    public static function generateLogNumber($logType, $userId){
        return $logType.addPrefixStringPad($userId,4,'0').date('Y').date('m').date('d').date('H').date('i').date('s');
    }
}
