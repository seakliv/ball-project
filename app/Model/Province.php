<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = [
        'id',
        'code',
        'name'
    ];

    public function districts(){
        return $this->hasMany(District::class);
    }
}
