<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BonusTopUp extends Model
{
    protected $table = "ball_bonus_top_ups";
}
