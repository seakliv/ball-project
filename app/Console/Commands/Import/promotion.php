<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use DB;
use App\Model\Promotion as modelPromotion;
class Promotion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:promotion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_ball_mysql')->table('promotions')->orderBy('id','asc')->chunk(100,function($promotion){
            $data = [];
            foreach($promotion as $row){
                $data[] = [
                    'id' => $row->id,
                    'name' => $row->name,
                    'from_date' => $row->from_date,
                    'to_date' => $row->to_date,
                    'target_order' => $row->target_order,
                    'bonus_order' => $row->bonus_order,
                    'running_text' => $row->running_text,
                    'created_at' => $row->created_at,
                    'updated_at' => $row->updated_at
                ];
            }
            modelPromotion::insert($data);
        });
    }
}
