<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use App\Model\UserAccount as userAccountModel;
use DB;
use App\Constants\Account;

class UserAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:userAccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_ball_mysql')->table('user_accounts')->orderBy('id','asc')->chunk(100,function($accounts){
            $data = [];
            foreach($accounts as $row){
                $data[] = [
                    'id' => $row->id,
                    'user_id' => $row->user_id,
                    'title' => $row->title,
                    'number' => $row->number,
                    'name' => $row->name,
                    'balance' => $row->balance,
                    'frozen' => $row->frozen,
                    'state' => $row->state,
                    'sort' => $row->sort,
                    'account_cat_id' => $row->account_cat_id,
                    'type_id' => $row->type_id,
                    'created_at' => $row->created_at,
                    'updated_at' => $row->updated_at
                ];
            }
            userAccountModel::insert($data);
        });
    }
    // public function accountName($type){
    //     $label = "";
    //     switch($type){
    //         case Account::CASH_ACCOUNT: $label = "Default Account"; break;
    //         case Account::THIRD_PARTY_ACCOUNT: $label = "Third Party Account"; break;
    //         case Account::PROFIT_ACCOUNT: $label = "Profit Account"; break;
    //         case Account::BONUS_ACCOUNT: $label = "Bonus Account"; break;
    //         default: $label = ""; break;
    //     }
    //     return $label;
    // }
}
