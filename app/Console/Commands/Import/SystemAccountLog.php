<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use DB;
use App\Model\SystemAccountLog as systemAccountLogModel;

class SystemAccountLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:systemAccountLog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_ball_mysql')->table('system_account_logs')->orderBy('id','asc')->chunk(100,function($logs){
            $data = [];
            foreach($logs as $row){
                $data[] = [
                    'id' => $row->id,
                    'amount' => $row->amount,
                    'balance' => $row->balance,
                    'abstract' => $row->abstract,
                    'user_abstract' => $row->user_abstract,
                    'log_number' => $row->log_number,
                    'log_type' => $row->log_type,
                    'to_type' => $row->to_type,
                    'account_id' => $row->account_id,
                    'to_account_id' => $row->to_account_id,
                    'manager_id' => $row->manager_id,

                    'created_at' => $row->created_at,
                    'updated_at' => $row->updated_at,
                ];
            }
            systemAccountLogModel::insert($data);
        });
    }
}
