<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use Artisan;

class All extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call('import:cycle');
        Artisan::call('import:gameRebate');
        Artisan::call('import:gameType');
        Artisan::call('import:order');
        Artisan::call('import:orderTemp');
        Artisan::call('import:promotion');
        Artisan::call('import:systemAccount');
        Artisan::call('import:systemAccountCategory');
        Artisan::call('import:systemAccountLog');
        Artisan::call('import:systemAccountType');
        Artisan::call('import:userAccount');
        Artisan::call('import:userAccountLog');
        Artisan::call('import:userAccountType');
    }
}
