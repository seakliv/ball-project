<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use App\Model\UserAccountType as userAccountTypeModel; 
use DB;

class UserAccountType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:userAccountType';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_ball_mysql')->table('user_account_types')
        ->orderBy('id', 'asc')
        ->chunk(10, function($accountTypes){
            $data = [];
            foreach($accountTypes as $row){
                $data[] = [
                    'id' => $row->id,
                    'name' => $row->name,
                    'created_at' => $row->created_at,
                    'updated_at' => $row->updated_at
                ];
            }
            userAccountTypeModel::insert($data);
        });
    }
}
