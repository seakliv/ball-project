<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use DB;
use App\Model\UserAccountLog as userAccountLogModel;

class UserAccountLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:userAccountLog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_ball_mysql')->table('user_account_logs')->orderBy('id','asc')->chunk(1000,function($logs){
            $data = [];
            foreach($logs as $row){
                $data[] = [
                    'id' => $row->id,
                    'log_type' => $row->log_type,
                    'is_transfer' => $row->is_transfer,
                    'amount' => $row->amount,
                    'balance' => $row->balance,
                    'commission' => $row->commission,
                    'win_money' => $row->win_money,
                    'to_type' => $row->to_type,
                    'abstract' => $row->abstract,
                    'log_number' => $row->log_number,

                    'user_id' => $row->user_id,
                    'account_id' => $row->account_id,
                    'to_user_id' => $row->to_user_id,
                    'to_account_id' => $row->to_account_id,
                    'manager_id' => $row->manager_id,

                    'created_at' => $row->created_at,
                    'updated_at' => $row->updated_at
                ];
            }
            userAccountLogModel::insert($data);
        });
    }
}
