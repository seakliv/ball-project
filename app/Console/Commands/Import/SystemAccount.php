<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use DB;
use App\Model\SystemAccount as modelSystemAccount;

class SystemAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:systemAccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_ball_mysql')->table('system_accounts')->orderBy('id','asc')->chunk(100,function($accounts){
            $data = [];
            foreach($accounts as $row){
                $data[] = [
                    'id' => $row->id,
                    'title' => $row->title,
                    'number' => $row->number,
                    'name' => $row->name,
                    'balance' => $row->balance,
                    'sort' => $row->sort,
                    'state' => $row->state,
                    'type_id' => $row->type_id,
                    'category_id' => $row->category_id,

                    'created_at' => $row->created_at,
                    'updated_at' => $row->updated_at
                ];
            }
            modelSystemAccount::insert($data);
        });
    }
}
