const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'public/global_assets/css/icons/icomoon/styles.min.css',
    'public/assets/css/bootstrap.min.css',
    'public/assets/css/bootstrap_limitless.min.css',
    'public/assets/css/layout.min.css',
    'public/assets/css/components.min.css',
    'public/assets/css/colors.min.css',
  ], 'public/assets/css/all.min.css');
