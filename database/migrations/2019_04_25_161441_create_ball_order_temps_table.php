<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallOrderTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_order_temps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('cycle_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedTinyInteger('user_type')->default(1);
            $table->string('ticket',30)->index();
            $table->boolean('is_settle')->default(0);
            $table->boolean('is_win')->default(0);
            $table->enum('win_prize',['3D','4D','5D','jackpot'])->nullable();
            $table->string('win_number',100)->default('[]')->nullable();
            $table->double('win_amount')->default(0.00);
            $table->double('amount')->default(0.00);
            $table->string('no1',2);
            $table->string('no2',2);
            $table->string('no3',2);
            $table->string('no4',2);
            $table->string('no5',2);
            $table->string('no6',2);

            $table->unsignedInteger('parent_id')->default(0)->index();
            $table->unsignedInteger('l1_id')->default(0)->index();
            $table->unsignedInteger('l2_id')->default(0)->index();
            $table->unsignedInteger('l3_id')->default(0)->index();
            $table->unsignedInteger('l4_id')->default(0)->index();
            $table->unsignedInteger('l5_id')->default(0)->index();
            $table->unsignedInteger('l6_id')->default(0)->index();
            $table->unsignedInteger('l7_id')->default(0)->index();
            $table->unsignedInteger('l8_id')->default(0)->index();

            $table->double('l1_rebate')->default(0.00);
            $table->double('l2_rebate')->default(0.00);
            $table->double('l3_rebate')->default(0.00);
            $table->double('l4_rebate')->default(0.00);
            $table->double('l5_rebate')->default(0.00);
            $table->double('l6_rebate')->default(0.00);
            $table->double('l7_rebate')->default(0.00);
            $table->double('l8_rebate')->default(0.00);

            $table->unsignedInteger('l1_du')->default(0);
            $table->unsignedInteger('l2_du')->default(0);
            $table->unsignedInteger('l3_du')->default(0);
            $table->unsignedInteger('l4_du')->default(0);
            $table->unsignedInteger('l5_du')->default(0);
            $table->unsignedInteger('l6_du')->default(0);
            $table->unsignedInteger('l7_du')->default(0);
            $table->unsignedInteger('l8_du')->default(0);

            $table->boolean('state')->default(1);
            $table->unsignedTinyInteger('game_type_id')->index();
            $table->boolean('is_test')->default(FALSE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_order_temps');
    }
}
