<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallPromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_promotions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('from_date',30);
            $table->string('to_date',30);
            $table->unsignedTinyInteger('target_order')->default(0);
            $table->unsignedTinyInteger('bonus_order')->default(0);
            $table->string('running_text')->nullable();
            $table->UnsignedInteger('game_type_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_promotions');
    }
}
