<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallUserAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_user_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            
            $table->string('title');
            $table->string('number');
            $table->string('name');
            $table->double('balance')->default(0.00);
            $table->double('frozen')->default(0.00);
            $table->boolean('state');
            $table->unsignedTinyInteger('sort')->default(0);

            $table->unsignedTinyInteger('account_cat_id')->nullable();
            $table->unsignedTinyInteger('type_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_user_accounts');
    }
}
