<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallGameTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_game_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',10);
            $table->string('name',20);
            $table->string('min',4);
            $table->string('max',4);
            $table->double('start_prize')->default(0);
            $table->double('current_prize')->default(0);
            $table->double('win_prize_5d')->default(0);
            $table->double('win_prize_4d')->default(0);
            $table->double('win_prize_3d')->default(0);
            $table->double('addon_percentage')->default(0);
            $table->double('order_price')->default(0);
            $table->double('lucky_draw')->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_game_types');
    }
}
