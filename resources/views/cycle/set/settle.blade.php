@extends('layouts.master')

@section('custom-css')
<style>
    @media only screen and (min-width:580px){
        .result-input .result{
            height: 70px;
            text-align: center;
            font-size: 20px;
        }
        .result-input input:not(:last-child){
            margin-right: 20px;
        }
        
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Cycle Set</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('cycles.set.index')}}" class="breadcrumb-item">{{ $settings['language']['LANG_LABEL_CYCLE_SN'] }}</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_CYCLE_SET'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    @canany('lottery-operation')
                    @if(isset($cycle))
                    <div class="progress" style="height: 30px;font-size: 14px">
                        <div class="progress-bar bg-teal" style="width: 0%" id="progress" 
                            data-route="{{route('ajax.cycles.result.settle')}}?cycle_sn={{$cycle->cycle_sn}}"
                            data-redirect-route="{{route('cycles.set.index')}}">
                            <span>Loading</span>
                        </div>
                    </div>
                    @else
                    <div>There is no cycle result to be settled!</div>
                    @endif
                    @endcanany
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')
<script>
    $(document).ready(function(){ 
        if($('#progress').length > 0){
            $.ajax({
                type: 'POST',
                url: $('#progress').data('route'),
                data: {},
                success: function(response){
                    if(response.success){
                        $('#progress').css('width','100%');
                        $('#progress span').html('100% complete');
                        window.location = $('#progress').data('redirect-route')
                    }
                },
                error: function(){

                },
            })
        }
    })
</script>
@endsection