@extends('layouts.master')

@section('custom-css')
<style>
    @media only screen and (min-width:580px){
        .result-input .result{
            height: 70px;
            text-align: center;
            font-size: 20px;
        }
        .result-input input:not(:last-child){
            margin-right: 20px;
        }
        
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_LABEL_CYCLE_CLOSE'] }}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('cycles.set.index')}}" class="breadcrumb-item">{{ $settings['language']['LANG_LABEL_CYCLE_SN'] }}</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_CYCLE_SET'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        {{ Form::open(['route' => 'cycles.result.set', 'method' => 'POST', 'id' => 'form-settle']) }}
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-7">
                    <div id="msg"></div>
                    @include('includes.error-msg')
                    @include('includes.success-msg')
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Cycle SN</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("cycle_sn",isset($cycle) ? $cycle->cycle_sn : '',
                                ["class" => "form-control",'readonly'])
                            }}
                        </div>
                    </div>

                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Prize Amount ($)</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("prize",isset($cycle) ? currencyFormat($cycle->prize * exchangeRateFromRielToDollar()) : '',
                                ["class" => "form-control",'readonly'])
                            }}
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback">
                        <label class="form-group-float-label font-weight-semibold is-visible">Result</label>
                        <div class="input-group result-input">
                            @for($i=1;$i<7;$i++)
                            {{ Form::text('no'.$i, isset($cycle) ? $cycle->result_number[$i-1] : old('no'.$i), ['required','class' => "text-center form-control result ", 'id' => 'no'.$i, 'placeholder' => 'No'.$i, 'min' => 01, 'max' => 35, 'maxlength' => 2]) }}
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="card-footer">
            <button type="button" id="generate-lotto-result" class="btn btn-light"><i class="icon-thumbs-up2 mr-1"></i> Generate Result</button>
            @canany('lottery-operation')
            <button type="button" id="save" class="btn btn-success" data-route="{{route('cycles.result.set')}}">
                <i class="icon-folder mr-1"></i> {{ $settings['language']['LANG_LABEL_CYCLE_POST'] }} 
            </button>
            @endcanany
            @if(isset($cycle) && $cycle->result_number && count($cycle->result_number) == 6)
            @canany('billing-operation')
                <button type="button" id="settle" class="btn btn-danger" data-route="{{route('cycles.result.settle')}}">
                    <i class="icon-folder mr-1"></i> Settle Result
                </button>
            @endcanany
            @endif
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('custom-js')
<script>
    $(document).ready(function(){ 
        $('#generate-lotto-result').click(function(){
            var arr = []
            while(arr.length < 6){
                var no = Math.floor(Math.random() * (35 - 1 + 1)) + 1;
                no = no.toString().length == 1 ? '0'+no : no;
                if(arr.indexOf(no) === -1) arr.push(no);
            }
            for(var i=1; i<7; i++){
                $('#no'+i).val(arr[i-1]);
            }
            
        })

        $('#save, #settle').click(function(){
            if(validateForm()){
                var route = $(this).data('route');
                $('#form-settle').attr('action',route);
                $('#form-settle').submit(); 
            }
        });

        function validateForm(){
            var numbers = [];
            for(var i=1;i<7;i++){
                numbers.push($('#no'+i).val());
                if($('#no'+i).val() == '' || $('#no'+i).val() < 01 || $('#no'+i).val() > 35){
                    $('#msg').html(
                        '<div class="alert bg-danger text-white alert-styled-left alert-dismissible">'+
                            '<button type="button" class="close" data-dismiss="alert"><span>×</span></button>'+
                            '<span class="font-weight-semibold">Oh snap!</span> Please fill all the lottery number from 01 to 35!'+
                        '</div>'
                    );
                    return false;
                }
            }
            $.unique(numbers.sort());
            if(numbers.length !=6){
                $('#msg').html(
                    '<div class="alert bg-danger text-white alert-styled-left alert-dismissible">'+
                        '<button type="button" class="close" data-dismiss="alert"><span>×</span></button>'+
                        '<span class="font-weight-semibold">Oh snap!</span> Please fill all the lottery number with unique number!'+
                    '</div>'
                );
            }
            return true;
        }
    })
</script>
@endsection