@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Game Promotion</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Game</span>
                <span class="breadcrumb-item active">Promotion</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($promotion))
        {{ Form::model($promotion,['route' => ['games.promotion.update',$promotion->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'games.promotion.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    @include('includes.error-msg')
                    @include('includes.success-msg')
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Promotion Name</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-key"></i></span>
                            </span>
                            {{Form::text("name", null,["class" => "form-control"])}}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">From Date</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("from_date",isset($promotion) ? date('Y-m-d H:i:s',$promotion->from_date) : date('Y-m-d 00:00:00'),
                                ["class" => "form-control datetimepicker"])
                            }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">To Date</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("to_date",isset($promotion) ? date('Y-m-d H:i:s',$promotion->to_date) : date('Y-m-d 00:00:00'),
                                ["class" => "form-control datetimepicker"])
                            }}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">Target Order Number</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-stack4"></i></span>
                            </span>
                            {{Form::number("target_order",null,["class" => "form-control"])}}
                            @if($errors->has('target_order'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('target_order'))
                            <span class="form-text text-danger">{{ $errors->first('target_order') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">Bonus Order Number</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-stack4"></i></span>
                            </span>
                            {{Form::number("bonus_order",null,["class" => "form-control"])}}
                            @if($errors->has('bonus_order'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('bonus_order'))
                            <span class="form-text text-danger">{{ $errors->first('bonus_order') }}</span>
                        @endif
                    </div>

                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Running Text</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-key"></i></span>
                            </span>
                            {{Form::text("running_text", null,["class" => "form-control"])}}
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        @canany(['game-promotion-modification'])
        <div class="card-footer">
            {{ Form::button('<i class="icon-folder mr-1"></i> '.$settings['language']['LANG_LABEL_POST'],['type' => 'submit', 'class' => 'btn btn-success']) }}
        </div>
        @endcanany
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection