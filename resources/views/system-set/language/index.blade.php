@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_MENU_LANG_SET'] }}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_SYSTEM_SET'] }}</span>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_LANG_SET'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.error-msg')
    @include('includes.success-msg')
    <div class="card">
        <div class="card-header pb-0">
            <div class="btn-group btn-group-sm float-right">
                <a href="{{route('system-set.languages.admin.index')}}" class="btn btn-success">System Language</a>
                <a href="{{route('system-set.languages.api.index')}}" class="btn btn-primary">App Language</a>
            </div>
        </div>
        @if(Route::currentRouteName() == 'system-set.languages.admin.index')
        {!! Form::open(['route' => 'system-set.languages.admin.store','method' => 'POST']) !!}
        @else
        {!! Form::open(['route' => 'system-set.languages.api.store','method' => 'POST']) !!}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="bg-slate-800">
                                    <th>Chinese</th>
                                    <th>English</th>
                                    <th>Khmer</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($languages) && $languages->count() > 0)
                                    @foreach($languages as $row)
                                        <tr>
                                            <td>{{Form::text('language['.$row->id.'][chinese]',$row->chinese,['class' => 'w-100 form-control form-control-sm'])}}</td>
                                            <td>{{Form::text('language['.$row->id.'][english]',$row->english,['class' => 'w-100 form-control form-control-sm'])}}</td>
                                            <td>{{Form::text('language['.$row->id.'][khmer]',$row->khmer,['class' => 'w-100 form-control form-control-sm'])}}</td>
                                        </tr>
                                    @endforeach
                                @else 
                                    <tr><td colspan="3">No Data</td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @canany(['exchange-rate-modification'])
            @if(isset($languages) && $languages->count() > 0)
            <div class="card-footer">
                {!! Form::button('<i class="icon-folder mr-1"></i> '.$settings['language']['LANG_LABEL_POST'], [ 'class' => 'btn btn-success', 'type' => 'submit']) !!}
            </div>
            @endif
        @endcanany
        {!! Form::close() !!}
    </div>
</div>
@endsection