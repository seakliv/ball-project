@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Location Set</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_SYSTEM_SET'] }}</span>
                <span class="breadcrumb-item active">Location Set</span>
                <span class="breadcrumb-item active">District</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.error-msg')
    @include('includes.success-msg')
    <div class="card">
        <div class="card-header">
            {{ Form::open(['route' => 'system-set.locations.districts.index', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
                <div class="form-group mb-2 mr-sm-2">
                    <div class="btn-group btn-group-sm w-100">
                        <a href="{{route('system-set.locations.provinces.index')}}" class="btn {{ Route::currentRouteName() == 'system-set.locations.provinces.index' ? 'btn-primary': 'btn-secondary' }}">Province</a>
                        <a href="{{route('system-set.locations.districts.index')}}" class="btn {{ Route::currentRouteName() == 'system-set.locations.districts.index' ? 'btn-primary': 'btn-secondary' }}">District</a>
                        <a href="{{route('system-set.locations.communes.index')}}" class="btn {{ Route::currentRouteName() == 'system-set.locations.communes.index' ? 'btn-primary': 'btn-secondary' }}">Commune</a>
                    </div>
                </div>
                <div class="form-group mb-2 mr-sm-2">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Province</div>
                        </div>
                        {{Form::select("province_id",$provinces, isset($_GET['province_id']) ? $_GET['province_id'] : null,
                            ["class" => "form-control input-sm", 'id' => 'province'])}}
                    </div>
                </div>
                <div class="form-group mb-2 mr-sm-2 ml-auto">
                    <div class="input-group input-group-sm">
                        <a href="{{ route('system-set.locations.districts.create') }}" class="btn btn-sm btn-primary w-100">
                            <i class="icon-plus3"></i> Add
                        </a>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="bg-slate-800">
                                    <th>Province Name</th>
                                    <th>District Name</th>
                                    <th>Code</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($districts) && $districts->count() > 0)
                                    @foreach($districts as $row)
                                        <tr>
                                            <td>{{ $row->province ? $row->province->name : '' }}</td>
                                            <td>{{ $row->name }}</td>
                                            <td>{{ $row->code }}</td>
                                            <td class="group-btn-action">
                                                @canany(['location-set-modification'])
                                                <div class="btn-group">
                                                    <a href="{{ route('system-set.locations.districts.edit',$row->id) }}" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2"><i class="icon-pencil7"></i> Edit</a>
                                                    <button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 delete" data-route="{{route('system-set.locations.districts.destroy',$row->id)}}"><i class="icon-trash"></i> Delete</a>
                                                </div>
                                                @endcanany    
                                            </td>
                                        </tr>
                                    @endforeach
                                @else 
                                    <tr><td colspan="4">No Data</td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if(isset($districts) && $districts->count() >0)
        <div class="card-footer">
            @if($districts->hasMorePages())
            <div class="mb-2">
                {!! $districts->appends(Input::except('page'))->render() !!}
            </div>
            @endif
            <div>
                Showing {{ $districts->firstItem() }} to {{ $districts->lastItem() }}
                of  {{$districts->total()}} entries
            </div>
        </div>
        @endif
    </div>
</div>
@endsection

@section('custom-js')
<script>
    $(document).ready(function(){
        if($('#province').length > 0){
            $('#province').change(function(){
                $('#province').parents('form').submit();
            });
        }
    })
</script>
@endsection