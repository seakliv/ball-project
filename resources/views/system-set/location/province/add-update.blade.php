@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Province</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('system-set.locations.provinces.index') }}" class="btn btn-sm btn-primary">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('system-set.locations.provinces.index')}}"  class="breadcrumb-item">{{ $settings['language']['LANG_MENU_SYSTEM_SET'] }}</span>
                <a href="{{route('system-set.locations.provinces.index')}}"  class="breadcrumb-item active">Location Set</a>
                <a href="{{route('system-set.locations.provinces.index')}}"  class="breadcrumb-item active">Province</a>
                <span class="breadcrumb-item active">{{ isset($province) ? 'Edit' : 'Add'}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($province))
        {{ Form::model($province,['route' => ['system-set.locations.provinces.update',$province->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'system-set.locations.provinces.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('name')?'text-danger':'' }}">Province Name</label>
                        <div class="position-relative">
                            {{Form::text("name",null,
                                ["class" => "form-control ".($errors->has('name')?'border-danger':''),"placeholder" => "Enter province name"])
                            }}
                            @if($errors->has('name'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('name'))
                            <span class="form-text text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Province Code</label>
                        <div class="position-relative">
                            {{Form::text("code",null,
                                ["class" => "form-control ","placeholder" => "Enter province code"])
                            }}
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> {{$settings['language']['LANG_LABEL_POST']}}
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection
