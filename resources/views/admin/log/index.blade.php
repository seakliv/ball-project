@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_MENU_MANAGER_LOG'] }}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{ route('managers.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_LABEL_MANAGER'] }}</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_MANAGER_LOG'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    {{ Form::open(['route' => 'managers.logs.index', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_MANAGER'] }}</div>
                </div>
                {{Form::select("manager_id",$managers, isset($_GET['manager_id']) ? $_GET['manager_id'] : null,["class" => "form-control input-sm", 'id' => 'manager'])}}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_START_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['begin_time']) ? $_GET['begin_time'] : date('Y-m-d 00:00:00',strtotime('-7 days'))}}" name="begin_time">
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_END_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['end_time']) ? $_GET['end_time'] : date('Y-m-d 00:00:00')}}" name="end_time">
            </div>
        </div>
        {{Form::submit('Search',['class' => 'btn btn-sm btn-primary mb-2 mr-2'])}}
        @canany('view-manager-log')
            <a href="{{route('managers.logs.index')}}" class="btn btn-warning btn-sm mb-2">Reset</a>
        @endcanany
    {{ Form::close() }}
    
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>{{ $settings['language']['LANG_LABEL_MANAGER'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_CONTENT'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACTION_DATETIME'] }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($logs->count() > 0)
                        @foreach($logs as $key => $row)
                        <tr>
                            <td>{{ $row->manager ? $row->manager->username : '' }}</td>
                            <td>
                                @foreach($row->content as $key => $value)
                                    {{ $key }} : {{ $value}} <br>
                                @endforeach
                            </td>
                            <td>{{ $row->created_at }}</td>
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="3">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if($logs->hasMorePages())
                <div class="mb-2">
                    {!! $logs->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{($logs->currentpage()-1)*$logs->perpage()+1}} to {{$logs->currentpage()*$logs->perpage()}}
                of  {{$logs->total()}} entries
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection

