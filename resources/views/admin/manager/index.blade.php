@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_MENU_MANAGER_LIST'] }}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            @canany(['add-new-manager-account'])
            <div class="d-flex justify-content-center">
                <a href="{{ route('managers.create') }}" class="btn btn-link btn-primary btn-sm btn-float text-default p-2"  data-popup="tooltip" title="" data-placement="bottom" data-original-title="Add New">
                    <i class="icon-user-plus text-white"></i>
                </a>
            </div>
            @endcanany
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="#" class="breadcrumb-item active">{{ $settings['language']['LANG_LABEL_MANAGER'] }}</a>
                <span class="breadcrumb-item active">List</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @include('includes.success-msg')
        @include('includes.error-msg')
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        <th>{{ $settings['language']['LANG_LABEL_USERNAME'] }}</th>
                        <th>{{ $settings['language']['LANG_MENU_MANAGER_ROLE'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_STATE'] }}</th>
                        <th>Last Login</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($managers) && $managers->count() > 0) 
                        @foreach($managers as $key => $row)
                        <tr>
                            <td>{{($managers->perPage() * ($managers->currentPage() - 1)) + $key + 1}}</td>
                            <td>{{ $row->username }} {{ $row->nick_name ? '('.$row->nick_name.')' : '' }}</td>
                            <td>{{ $row->roles && $row->roles->count() > 0 ? $row->roles[0]->name : '—' }}</td>
                            <td>
                                @if($row->state == 1)
                                    <span class="badge badge-success">Active</span>
                                @else 
                                    <span class="badge badge-danger">Deactive</span>
                                @endif
                            </td>
                            <td>{{ $row->last_login != 0 ? date('Y-m-d H:i:s',$row->last_login) : '0000-00-00 00:00:00' }}</td>
                            <td class="group-btn-action">
                                @canany(['manager-account-modification'])
                                    <div class="btn-group">
                                        <a href="{{ route('managers.edit',$row->id) }}" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2"><i class="icon-pencil7"></i> {{$settings['language']['LANG_LABEL_EDIT']}}</a>
                                        {{-- <button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 delete" data-route="{{route('managers.destroy',$row->id)}}"><i class="icon-trash"></i> Delete</a> --}}
                                    </div>
                                @endcanany
                            </td>
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="3">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(isset($managers))
        <div class="card-footer">
            @if($managers->hasMorePages())
                <div class="mb-2">
                    {!! $managers->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{($managers->currentpage()-1)*$managers->perpage()+1}} to {{$managers->currentpage()*$managers->perpage()}}
                of  {{$managers->total()}} entries
            </div>
        </div>
        @endif
    </div>
</div>
@endsection