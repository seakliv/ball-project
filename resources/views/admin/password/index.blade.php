@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{$settings['language']['LANG_LABEL_EDIT_PASS']}}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_LABEL_MANAGER'] }}</span>
                <span class="breadcrumb-item active">{{$settings['language']['LANG_LABEL_EDIT_PASS']}}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    @include('includes.success-msg')
                    @include('includes.error-msg')
                </div>
                <div class="col-lg-6">
                    <h4>{{$settings['language']['LANG_LABEL_EDIT_PASS']}}</h4>
                    {{ Form::open(['route' => ['managers.password.update',$manager->id], 'method' => 'PUT']) }}
                        @csrf
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('old_login_password')?'text-danger':'' }}">{{$settings['language']['LANG_TIPS_ENTER_OLDPASS']}}</label>
                            <div class="position-relative">
                                {{Form::password("old_login_password",
                                    ["class" => "form-control ".($errors->has('old_login_password')?'border-danger':''),"placeholder" => $settings['language']['LANG_TIPS_ENTER_OLDPASS_MAT'],'autocomplete' => 'off'])
                                }}
                                @if($errors->has('old_login_password'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('old_login_password'))
                                <span class="form-text text-danger">{{ $errors->first('old_login_password') }}</span>
                            @endif
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('login_password')?'text-danger':'' }}">{{$settings['language']['LANG_TIPS_ENTER_NEWPASS']}}</label>
                            <div class="position-relative">
                                {{Form::password("login_password",
                                    ["class" => "form-control ".($errors->has('login_password')?'border-danger':''),"placeholder" => $settings['language']['LANG_TIPS_ENTER_LEAST_6'], 'autocomplete' => 'off'])
                                }}
                                @if($errors->has('login_password'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('login_password'))
                                <span class="form-text text-danger">{{ $errors->first('login_password') }}</span>
                            @endif
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('login_password_confirmation ')?'text-danger':'' }}">{{$settings['language']['LANG_LABEL_SURE_PASS']}}</label>
                            <div class="position-relative">
                                {{Form::password("login_password_confirmation",
                                    ["class" => "form-control ".($errors->has('login_password_confirmation')?'border-danger':''),"placeholder" => $settings['language']['LANG_TIPS_ENTER_LEAST_6'], 'autocomplete' => 'off'])
                                }}
                                @if($errors->has('login_password_confirmation'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('login_password_confirmation'))
                                <span class="form-text text-danger">{{ $errors->first('login_password_confirmation') }}</span>
                            @endif
                        </div>
                        {!! Form::hidden('type','login_password') !!}
                        <button type="submit" class="btn btn-success">
                            <i class="icon-folder mr-1"></i> {{ $settings['language']['LANG_LABEL_POST'] }}
                        </button>
                    {{ Form::close() }}
                </div>
                <div class="col-lg-6">
                    <h4>{{$settings['language']['LANG_LABEL_EDIT_PAYPASS']}}</h4>
                    {{ Form::open(['route' => ['managers.password.update',$manager->id], 'method' => 'PUT']) }}
                        @csrf
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('old_pay_pass')?'text-danger':'' }}">{{$settings['language']['LANG_LABEL_OLD_PAYPASS']}}</label>
                            <div class="position-relative">
                                {{Form::password("old_pay_pass",
                                    ["class" => "form-control ".($errors->has('old_pay_pass')?'border-danger':''),"placeholder" => $settings['language']['LANG_TIPS_ENTER_OLDPASS_MAT'],'autocomplete' => 'off'])
                                }}
                                @if($errors->has('old_pay_pass'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('old_pay_pass'))
                                <span class="form-text text-danger">{{ $errors->first('old_pay_pass') }}</span>
                            @endif
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('pay_pass')?'text-danger':'' }}">{{$settings['language']['LANG_LABEL_NEW_PAYPASS']}}</label>
                            <div class="position-relative">
                                {{Form::password("pay_pass",
                                    ["class" => "form-control ".($errors->has('pay_pass')?'border-danger':''),"placeholder" => $settings['language']['LANG_TIPS_ENTER_LEAST_6'],'autocomplete' => 'off'])
                                }}
                                @if($errors->has('pay_pass'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('pay_pass'))
                                <span class="form-text text-danger">{{ $errors->first('pay_pass') }}</span>
                            @endif
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('pay_pass_confirmation ')?'text-danger':'' }}">{{$settings['language']['LANG_LABEL_SURE_PASS']}}</label>
                            <div class="position-relative">
                                {{Form::password("pay_pass_confirmation",
                                    ["class" => "form-control ".($errors->has('pay_pass_confirmation')?'border-danger':''),"placeholder" => $settings['language']['LANG_TIPS_ENTER_LEAST_6'], 'autocomplete' => 'off'])
                                }}
                                @if($errors->has('password_confirmation'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('pay_pass_confirmation'))
                                <span class="form-text text-danger">{{ $errors->first('pay_pass_confirmation') }}</span>
                            @endif
                        </div>
                        {!! Form::hidden('type','pay_password') !!}
                        <button type="submit" class="btn btn-success">
                            <i class="icon-folder mr-1"></i> {{ $settings['language']['LANG_LABEL_POST'] }}
                        </button>
                    {{ Form::close() }}
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection